## sniffProbes

### A light weight, basic program to sniff probe requests from an interface (in monitor/promiscuous mode).

    

Currently the output is designed to be used with unix filters
or piped to a higher level language (it isn't exacly human friendly).
This does however mean it can be compiled and run on almost any
unix device that has an interface with monitor mode capability
(such as low pwr IoT devices), and deployed anywhere.

e.g

    ./sniffProbes wlan1mon | egrep -v '(SOME_MAC)|(ANOTHER_MAC)'

or

    ./sniffProbes wlan1mon | ./map_wigle_locations.py

or

    ./sniffProbes wlan1mon | egrep '^SOME_MAC' | ./geofence_alert.sh

etc etc


Potential uses;

Geo-fencing and alerts - give your pet (or a person) wifi, when it leaves 
                         the network we should catch a probe and run a 
                         script accordingly.


Aid to social eng      - Use wigle to map the SSIDs a person connects to
                         and create a polygon of their living area - they
                         likely live somewhere on, or within the polygon.

A (!rough!) tracker    - An approximate distance between radios is given
                         using free space path loss, pipe a few of these
                         over netcat, and triangulate.



** NOTE **

The FSPL equation is uncalibrated for antenna gain and could easily be 
improved upon. Metal obstructions can boost a signal, others can degrade
it - add that to humidity and intense sunlight to understand why it 
fluctuates.  Do some filtering on this, and calibrate to improve it.


Compile with: gcc sniffProbes.c -o sniffProbes -lm

Then the typical;

    ifconfig wlanX down
    airmon-ng check kill
    ifconfig wlanX up
    airmon-ng start wlanX
    ./sniffProbes wlanXmon

![screenshot](img/img.png)
        