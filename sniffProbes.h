/*
MIT License

Copyright (c) 2018 Sam Clarke

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include <stdint.h>

#define TRUE   1
#define FALSE  0
#define NOFLAG 0

#define TIME_BUFF_SZ 128
#define SSID_BUFF_SZ 32
#define MAC_BUFF_SZ  32
// Needs to be sized for arbitrary protocols
#define PKT_BUFF_SZ  65536
// 802.11 management protocols
#define IEEE80211_STYPE_ASSOC_REQ       0x00
#define IEEE80211_STYPE_ASSOC_RESP      0x01
#define IEEE80211_STYPE_REASSOC_REQ     0x02
#define IEEE80211_STYPE_REASSOC_RESP    0x03
#define IEEE80211_STYPE_PROBE_REQ       0x04
#define IEEE80211_STYPE_PROBE_RESP      0x05
#define IEEE80211_STYPE_BEACON          0x08
#define IEEE80211_STYPE_ATIM            0x09
#define IEEE80211_STYPE_DISASSOC        0x0A
#define IEEE80211_STYPE_AUTH            0x0B
#define IEEE80211_STYPE_DEAUTH          0x0C
#define IEEE80211_STYPE_ACTION          0x0D
// 802.11 frame offsets
#define FRAME_CTL_OFFSET 26
#define SSID_LEN_OFFSET  51
#define SSID_CHR_OFFSET  52
#define MAC_ADDR_OFFSET  36
#define DEST_ADDR_OFFSET 42
#define RSSI_OFFSET      22
#define FREQ_OFFSET      19

typedef struct _req *Request;

typedef struct _req{
    // String timestamp of
    char timeStamp[TIME_BUFF_SZ];
    // Device MAC
    char clientMAC[MAC_BUFF_SZ];
    // Requesting station
    char SSID[SSID_BUFF_SZ];
    // On frequency
    uint16_t frequency;
    // With signal strength
    int8_t RSSI;
    // Which imples
    float distance;
} req;

// The main parser
Request parseRaw( uint8_t *buff, uint16_t buffSize );
// A boolean for the destination MAC
int isBroadcast( uint8_t *buff );
// Handy for conversions
uint64_t macU8ToU64( uint8_t *mac );
// A rough distance using uncalibrated FSPL equations
float signalToDistance( int8_t RSSI, uint16_t frequency );
