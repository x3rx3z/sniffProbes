/*
    sniffProbes.c/h

    A light weight, basic program to dump probe requests from an
    interface (in monitor/promiscuous mode).

    Currently the output is designed to be used with unix filters
    or piped to a higher level language (it isn't exacly human friendly).
    This does however mean it can be compiled and run on almost any
    unix device that has an interface with monitor mode capability
    (such as low pwr IoT devices), and deployed anywhere.

    e.g

        ./sniffProbes wlan1mon | egrep -v '(SOME_MAC)|(ANOTHER_MAC)'

        or

        ./sniffProbes wlan1mon | ./map_wigle_locations.py

        or

        ./sniffProbes wlan1mon | egrep '^SOME_MAC' | ./geofence_alert.sh

        etc etc


    Potential uses;

    Geo-fencing and alerts - give your pet (or a person) wifi, when it leaves 
                             the network we should catch a probe and run a 
                             script accordingly.


    Aid to social eng      - Use wigle to map the SSIDs a person connects to
                             and create a polygon of their living area - they
                             likely live somewhere on, or within the polygon.

    A (!rough!) tracker    - An approximate distance between radios is given
                             using free space path loss, pipe a few of these
                             over netcat, and triangulate.



    ** NOTE **

    The FSPL equation is uncalibrated for antenna gain and could easily be 
    improved upon. Metal obstructions can boost a signal, others can degrade
    it - add that to humidity and intense sunlight to understand why it 
    fluctuates.  Do some filtering on this, and calibrate to improve it.



MIT License

Copyright (c) 2018 Sam Clarke

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
// x86 PRIX64 - uint64_t compat
#include <inttypes.h>
// isalnum
#include <ctype.h>
// memset
#include <string.h>
// FSPL equations
#include <math.h>
// Socket
#include <sys/socket.h>
// ETH_P_ALL
#include <netinet/if_ether.h>
// Interface options
#include <net/if.h>
#include <sys/ioctl.h>
// htons() 
#include <netinet/ip.h>
// Local
#include "sniffProbes.h"

/*

    Listen on the specified monitor interface, when an 802.11
    probe request if found, scrape it for MAC/BSSID/RSSI.

    This program is designed to be light weight for use with 
    Unix filters.

*/
int main( int argc, char **argv ) {
    
    // At the moment, the monitor interface
    // is provided by the user.
    char *interface = "";    
    if( argc == 2 ) {
        interface = argv[1];
    } else {
        // Give a little help.
        fprintf(stderr, "Provide exactly one argument, "
                        "the monitor interface dev name.\n"
                        "e.g\n"
                        "./sniffProbes wlanXmon\n");
        fflush( stderr );
        return EXIT_FAILURE;
    }
    
    // Create a buffer for 802.11 packets
    uint8_t pktBuff[ PKT_BUFF_SZ ];

    // Setup a socket to sniff all protocols on the interface
    int sock = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if(sock < 0){
        fprintf(stderr, "[sniffProbes] Error creating socket, "
                        "are you running as root?\n");
        fflush( stderr );
        return EXIT_FAILURE;
    }
    
    // Setup interface - SECURITY RISK - constrain the size *interface
    struct ifreq interfaceOpts;
    strncpy(interfaceOpts.ifr_name, interface, strlen( interface ));
    ioctl(sock, SIOCGIFFLAGS, &interfaceOpts);
    
    // Ensure promiscuous mode is set - not really needed within spec
    interfaceOpts.ifr_flags |= IFF_PROMISC;
    int setOptsVal = setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, 
                                &interfaceOpts, sizeof(interfaceOpts));
    if(setOptsVal < 0) {
        fprintf( stderr, "[sniffProbes] Error setting promiscuous mode on "
                         "interface: %s\n", interface );
        fflush(  stderr );
        return EXIT_FAILURE;
    }

    // Bind to the interface - This is a network layer option, not phy, 
    // thus if there's a monitor interface running elsewhere, we'll still 
    // receive packets - sending also has no guarantee of phy level interface.
    setOptsVal = setsockopt( sock, SOL_SOCKET, SO_BINDTODEVICE, 
                             interface, strlen(interface) );
    if(setOptsVal < 0) {
        fprintf( stderr, "[sniffProbes] Error binding to specified "
                         "interface: %s\n", interface );
        fflush(  stderr );
        return EXIT_FAILURE;
    }
    
    // Main loop 
    while(TRUE) {
        // Receive any data from the broadcast medium (802.11)
        int recvDataSize = recvfrom( sock, pktBuff, PKT_BUFF_SZ,
                                     NOFLAG, NULL, NULL);
        if( recvDataSize ) {
            // Send off to parser 
            Request request = parseRaw(pktBuff, recvDataSize);
            
            // If it was a probe request
            if( request != NULL ){ 
                // Print in an easily filtered format for now
                fprintf(stdout, "%s|%s|%ddBm|%.02fm\n", 
                                request->clientMAC, 
                                request->SSID,
                                request->RSSI,
                                request->distance);
                fflush( stdout );
            }
            free( request);
        }
    }
    return EXIT_SUCCESS;
}


/*
    Parse 802.11 probe requests from raw socket data.
    Since this is an open, promiscuous socket..The data
    could really be anything.

    Considered a probe request if;
        
        - The correct frame protocol (802.11 probe request) 
          at the correct offset

        - A broadcast destination MAC at the correct offset

    This empirically seems to be enough, we want to keep the overhead low.

*/
Request parseRaw( uint8_t *buff, uint16_t buffSize ) {
    // Get the frame protocol out
    int16_t frameProtocol = (buff[FRAME_CTL_OFFSET] & 0xF0) >> 4;
    // If statements are eval'd in order, ordered for fast rejection.
    if( frameProtocol == IEEE80211_STYPE_PROBE_REQ &&
        isBroadcast( &buff[DEST_ADDR_OFFSET])      ){
        // Begin decoding the frame
        int i, validSSID;
        validSSID = TRUE;
        // Throw a new request onto the heap
        Request request = malloc( sizeof( Request ) );
        // Preformat the SSID with NULL chars
        memset(request->SSID, '\0', SSID_BUFF_SZ);
        memset(request->clientMAC, '\0', 16);
        // Check the SSID isn't blank or ovrflw
        uint8_t SSIDlen = buff[SSID_LEN_OFFSET];
	if( SSIDlen >= 0 && SSIDlen <= SSID_BUFF_SZ ) {
            if( SSIDlen == 0 ) {
                strcpy(request->SSID, "BROADCAST PROBE");
            } else {
	        // Copy SSID into request
                for(i = 0; i < SSIDlen; i++){
                    // If it's a printable char
                    if( isprint(buff[SSID_CHR_OFFSET + i]) && validSSID ) {
                        request->SSID[i] = buff[SSID_CHR_OFFSET + i];
                    // Otherwise it's something interesting. Seen non ASCII
                    // SSIDs in the wild.
                    } else {
                        // Convert it to a '\xNN' string rep
                        char hexByte[5];
                        snprintf(&hexByte[0], 5, "\\x%02X",
                                (unsigned char)buff[SSID_CHR_OFFSET + i]);
                        // Set the SSID to printable HEX
                        request->SSID[i] = hexByte[0];
                        request->SSID[i+1] = hexByte[1];
                        request->SSID[i+2] = hexByte[2];
                        request->SSID[i+3] = hexByte[3];
                        // Kick i fwd a few spots
                        i+=3;
                        validSSID = FALSE;
                    }
		}
            }
            // Now get client MAC as uint64
            uint64_t longMac = macU8ToU64( &buff[ MAC_ADDR_OFFSET ] );
            // Make a HEX string from it
            snprintf(request->clientMAC, 32, "%012" PRIX64, longMac);
            // Set the RSSI (dBm)
            request->RSSI = buff[ RSSI_OFFSET ] - 0xFF;
            // Set the frequency - not that interesting, doppler would be nice
            request->frequency  = ( buff[ FREQ_OFFSET ] ) << 8;
            request->frequency |= ( buff[ FREQ_OFFSET+1 ] );
            // set the distance estimate - rough, but still informative
            request->distance = signalToDistance(request->RSSI, 
                                                 request->frequency);
            return request;
        } else {
            // SSID length was outside spec
            return NULL;
        }
    } else {
        // Frame protocol other than probe request
        return NULL;
    }
} 

/*
    Check 6 bytes of a buffer for a broadcast MAC.
    uint8_t *buff should point to the first byte
    of the MAC. 
*/
int isBroadcast( uint8_t *buff ) {
     if( buff[5] == 0xFF && buff[4] == 0xFF && 
         buff[3] == 0xFF && buff[2] == 0xFF && 
         buff[1] == 0xFF && buff[0] == 0xFF ) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/*
    Shift a MAC address into a 64 bit unsigned int.
    Useful for converting to a string.
*/
uint64_t macU8ToU64( uint8_t *mac ) {
    uint64_t macint;
    // MAC bytes into a long
    macint =  (uint64_t)mac[0] << (8*5);
    macint |= (uint64_t)mac[1] << (8*4);
    macint |= (uint64_t)mac[2] << (8*3);
    macint |= (uint64_t)mac[3] << (8*2);
    macint |= (uint64_t)mac[4] <<  8;
    macint |= (uint64_t)mac[5];
    return macint;
}

/*
    Get an estimate of device distance from freq and RSSI.
    Uses "free space path loss" equation.

    https://en.wikipedia.org/wiki/Free-space_path_loss

    In practice the is fairly rough.  A high gain antenna throw this 
    off completely.  Further, a small obstruction such as a wall
    can cause a large error in distance (metal objects can increase the signal)
    Even humidity and sunlight can throw this off somewhat


    ** CHANGE 03022018 **
    Since the frequency is always 2410MHz, we can precalculate the 20log(f) and 
    20log(4pi/c) components and reduce the total equation to;

    10^(1+[(-RSSI-40.09)/20]) = d (meters)

    The 1+ in the exponent describes the ~1dB gain in a standard Alfa antenna
    over that of a mobile phone (or other device). - Empirically tested.
*/
float signalToDistance( int8_t RSSI, uint16_t frequency ) {
    //float distance = (27.55-RSSI-(20*log10(frequency)))/20.00;
    //return (float)pow(10, distance); 
    float exp = 1+(-(RSSI)-40.09)/20;
    return pow(10, exp);
}
